<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <title>User page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet"
          href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <style>
        .top-margin {
            margin-top: 48px;
        }
    </style>
    <script
            src="https://code.jquery.com/jquery-3.4.0.min.js"
            integrity="sha256-BJeo0qm959uMBGb65z40ejJYGSgR7REI4+CW1fNKwOg="
            crossorigin="anonymous"></script>
</head>
<body class="top-margin">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-lg-offset-3">
                <form id="user" method="post">
                    <div class="form-group top-margin">
                        <label for="first_name">First name</label>
                        <input type="text" class="form-control" id="first_name" placeholder="First name">
                    </div>

                    <div class="form-group top-margin">
                        <label for="last_name">Last name</label>
                        <input type="text" class="form-control" id="last_name" placeholder="Last name">
                    </div>

                    <div class="form-group top-margin">
                        <label for="email">Email</label>
                        <input type="text" class="form-control" id="email" placeholder="Email">
                    </div>

                    <button type="submit" class="btn btn-primary btn-block top-margin">Apply</button>
                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-5 col-lg-offset-3">
                <div id="response">
                    <div class="form-group top-margin">
                        <label for="first_name">Response message</label>
                        <textarea class="form-control" id="responseMessage" placeholder="Response message" rows="5"></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function() {

            $('#user').on('submit', function(e) {
                e.preventDefault()

                var data = {
                    "first_name": $('#first_name').val(),
                    "last_name": $('#last_name').val(),
                    "email": $('#email').val()
                };

                $.ajax({
                    type: "POST",
                    url: '/users',
                    dataType: 'json',
                    contentType: "application/json",
                    processData: false,
                    data: JSON.stringify(data),
                    success: function(response) {
                        checkResponse(response);
                    },
                    error: function(){
                        clearInterval(interval);
                        $('#message').html('<span style="color:red">Connection problems</span>');
                    }
                })
            });

            function checkResponse(hash) {
                var interval = setInterval(function(){
                    $.ajax({
                        type: "POST",
                        url: '/users/byHash',
                        dataType: 'json',
                        contentType: "application/json",
                        processData: false,
                        data: JSON.stringify(hash),
                        success: function(response) {
                            if(response.xml) {
                                clearInterval(interval);
                                $("#responseMessage").val(response.xml);
                            }
                        },
                        error: function(){
                            clearInterval(interval);
                            $('#message').html('<span style="color:red">Connection problems</span>');
                        }
                    })
                }, 1000);
            }
        });
    </script>
</body>
</html>