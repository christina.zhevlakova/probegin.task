<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateUsersFunctions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE OR REPLACE FUNCTION get_full_name(first_name TEXT, last_name TEXT) RETURNS TEXT AS $$
                SELECT first_name || ' ' || last_name;
            $$ LANGUAGE SQL
        ");

        DB::statement("CREATE OR REPLACE FUNCTION insert_user_by_json(data JSON) RETURNS XML AS
            $$
            DECLARE
                result XML := NULL;
            BEGIN
                IF data->>'first_name' ISNULL OR data->>'first_name' = '' THEN
                    RAISE EXCEPTION 'First name is not defined!';
                END IF;
                IF data->>'last_name' ISNULL OR data->>'last_name' = '' THEN
                    RAISE EXCEPTION 'Last name is not defined!';
                END IF;
                IF data->>'email' ISNULL OR data->>'email' = '' THEN
                    RAISE EXCEPTION 'Email is not defined!';
                END IF;
            
                PERFORM FROM users WHERE email = data->>'email';
                IF FOUND THEN
                    RAISE EXCEPTION 'Email \"%\" already exists!', data->>'email';
                END IF;
            
                INSERT INTO users(email, name_first, name_last) VALUES (data->>'email', data->>'first_name', data->>'last_name')
                RETURNING xmlforest(id, email,
                    name_first AS first_name,
                    name_last AS last_name,
                    name_full AS full_name
                ) INTO result;
                
                RETURN result;
            END
            $$ LANGUAGE plpgsql
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP FUNCTION insert_user_by_json(JSON)");
        DB::statement("DROP FUNCTION get_full_name(TEXT, TEXT)");
    }
}
