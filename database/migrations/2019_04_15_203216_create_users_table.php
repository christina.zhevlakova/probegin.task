<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE OR REPLACE FUNCTION tg_users_before() RETURNS trigger AS $$
            BEGIN
                NEW.name_full = get_full_name(NEW.name_first, NEW.name_last);
            
                RETURN NEW;
            END;
            $$ LANGUAGE plpgsql
        ");

        DB::statement("CREATE TABLE users (
            id SERIAL PRIMARY KEY,
            email VARCHAR(100) NOT NULL UNIQUE,
            name_first VARCHAR(100) NOT NULL,
            name_last VARCHAR(100) NOT NULL,
            name_full VARCHAR(255) NOT NULL
        )");

        DB::statement("CREATE TRIGGER users_before BEFORE INSERT OR UPDATE ON users FOR EACH ROW EXECUTE PROCEDURE tg_users_before()");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP TABLE users");
        DB::statement("DROP FUNCTION tg_users_before()");
    }
}
