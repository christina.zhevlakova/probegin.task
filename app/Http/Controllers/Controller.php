<?php

namespace App\Http\Controllers;

use App\Actions\Users\CreateUserCommandAction;
use App\Actions\Users\GetUserDataAction;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

/**
 * Class Controller
 * @package App\Http\Controllers
 */
class Controller extends BaseController
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('index');
    }

    /**
     * @param \Illuminate\Http\Request                   $request
     * @param \App\Actions\Users\CreateUserCommandAction $action
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\InvalidArgumentException
     */
    public function createUser(Request $request, CreateUserCommandAction $action)
    {
        return response()->json([
            'hash' => $action->run($request->all())
        ]);
    }

    /**
     * @param \Illuminate\Http\Request             $request
     * @param \App\Actions\Users\GetUserDataAction $action
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserByHash(Request $request, GetUserDataAction $action)
    {
        return response()->json([
            'xml' => $action->run($request->hash)
        ]);
    }
}
