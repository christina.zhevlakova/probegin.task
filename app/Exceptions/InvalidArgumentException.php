<?php

namespace App\Exceptions;

/**
 * Class InvalidArgumentException
 * @package App\Exceptions
 */
class InvalidArgumentException extends \Exception
{
    /**
     * @var string
     */
    protected $message = "Invalid argument";

    /**
     * @var int
     */
    protected $code = 400;
}