<?php

namespace App\Console\Commands\Users;

use App\Actions\Compressors\CompressorFactory;
use App\Actions\Users\CreateUserByJsonAction;
use Bschmitt\Amqp\Amqp;
use Illuminate\Console\Command;

/**
 * Class CreateRequestQueueCommand
 * @package App\Console\Commands\Users
 */
class CreateRequestQueueCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'queue:users:create:listen';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Listen Users queue';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param \Bschmitt\Amqp\Amqp                       $amqp
     * @param \App\Actions\Users\CreateUserByJsonAction $createUserByJsonAction
     *
     * @throws \Bschmitt\Amqp\Exception\Configuration
     */
    public function handle(Amqp $amqp, CreateUserByJsonAction $createUserByJsonAction)
    {
        $amqp->consume('usersCreateQueue', function ($message, $resolver) use ($createUserByJsonAction) {

            $data = unserialize($message->body);

            try {
                if(!empty($data['hash']) && !empty($data['message'])) {
                    $createUserByJsonAction->run(
                        $data['hash'],
                        json_decode(
                            CompressorFactory::decompress($data['message']),
                            true
                        )
                    );
                }
            }
            catch(\Exception $e) {

            }
            $resolver->acknowledge($message);
        });
    }
}
