<?php

namespace App\Console\Commands\Users;

use App\Actions\Compressors\CompressorFactory;
use Bschmitt\Amqp\Amqp;
use Illuminate\Console\Command;
use Predis\Client;

/**
 * Class CreateResponseQueueCommand
 * @package App\Console\Commands\Users
 */
class CreateResponseQueueCommand extends Command
{
    const DEFAULT_USER_XML_TTL = 3600;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'queue:users:created:listen';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Listen Users created queue';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param \Bschmitt\Amqp\Amqp $amqp
     * @param \Predis\Client      $redis
     *
     * @throws \Bschmitt\Amqp\Exception\Configuration
     */
    public function handle(Amqp $amqp, Client $redis)
    {
        $amqp->consume('usersCreatedQueue', function ($message, $resolver) use ($redis) {
            $data = unserialize($message->body);

            $redis->set(
                config('database.redis.prefix').':'.$data['hash'],
                CompressorFactory::decompress($data['message'])
            );

            $redis->expire(
                config('database.redis.prefix').':'.$data['hash'],
                self::DEFAULT_USER_XML_TTL
            );

            $resolver->acknowledge($message);
        });
    }
}
