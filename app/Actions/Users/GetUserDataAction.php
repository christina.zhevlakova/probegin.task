<?php

namespace App\Actions\Users;

use Predis\Client;

/**
 * Class GetUserDataAction
 * @package App\Actions\Users
 */
class GetUserDataAction
{
    /**
     * @var \Predis\Client
     */
    private $redis;

    /**
     * GetUserDataAction constructor.
     *
     * @param \Predis\Client $redis
     */
    public function __construct(Client $redis)
    {
        $this->redis = $redis;
    }

    /**
     * @param string $hash
     *
     * @return string
     */
    public function run(string $hash): string
    {
        if(!$xml = $this->redis->get(config('database.redis.prefix').':'.$hash)) {
            return '';
        }

        $this->redis->del([config('database.redis.prefix').':'.$hash]);

        return $xml;
    }
}
