<?php

namespace App\Actions\Users;

use App\Events\Users\UserCreatedEvent;
use App\Exceptions\InvalidArgumentException;
use Illuminate\Database\DatabaseManager;

/**
 * Class CreateUserByJsonAction
 * @package App\Actions\Users
 */
class CreateUserByJsonAction
{
    /**
     * @var \Illuminate\Database\DatabaseManager
     */
    private $db;

    /**
     * CreateUserByJsonAction constructor.
     *
     * @param \Illuminate\Database\DatabaseManager $db
     */
    public function __construct(DatabaseManager $db)
    {
        $this->db = $db;
    }

    /**
     * @param string $hash
     * @param array  $data
     *
     * @throws \App\Exceptions\InvalidArgumentException
     */
    public function run(string $hash, array $data): void
    {
        $this->validateData($data);

        $result = $this->db->select("SELECT insert_user_by_json(:data) AS xml", [
            'data' => json_encode($data),
        ]);

        event(new UserCreatedEvent([
            'hash' => $hash,
            'xml' => '<?xml version="1.0" encoding="UTF-8"?>' . $result[0]->xml,
        ]));

        return;
    }

    /**
     * @param array $data
     *
     * @throws \App\Exceptions\InvalidArgumentException
     */
    private function validateData(array $data): void
    {
        if(empty($data['first_name']) || empty($data['last_name']) || empty($data['email'])) {
            throw new InvalidArgumentException('Empty required data');
        }

        if($this->db->select("SELECT id FROM users WHERE email = :email", [
            'email' => $data['email'],
        ])) {
            throw new InvalidArgumentException('User already exists');
        }
    }
}
