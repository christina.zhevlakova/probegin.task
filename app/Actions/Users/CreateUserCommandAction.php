<?php

namespace App\Actions\Users;

use App\Actions\Queue\SendCompressedMessageAction;

/**
 * Class CreateUserCommandAction
 * @package App\Actions\Users
 */
class CreateUserCommandAction
{
    /**
     * @var \App\Actions\Queue\SendCompressedMessageAction
     */
    private $sendCompressedMessageAction;

    /**
     * CreateUserCommandAction constructor.
     *
     * @param \App\Actions\Queue\SendCompressedMessageAction $sendCompressedMessageAction
     */
    public function __construct(SendCompressedMessageAction $sendCompressedMessageAction)
    {
        $this->sendCompressedMessageAction = $sendCompressedMessageAction;
    }

    /**
     * @param array $data
     *
     * @return string
     * @throws \App\Exceptions\InvalidArgumentException
     */
    public function run(array $data): string
    {
        $message = json_encode($data);
        $hash = md5($message.microtime(true));

        $this->sendCompressedMessageAction->run('usersCreateQueue', $hash, $message);

        return $hash;
    }
}
