<?php

namespace App\Actions\Compressors;

/**
 * Class UncompressHandler
 * @package App\Actions\Compressors
 */
class UncompressHandler implements CompressorInterface
{
    /**
     * @param string $text
     *
     * @return string
     */
    public function compress(string $text): string
    {
        return base64_encode($text);
    }

    /**
     * @param string $text
     *
     * @return string
     */
    public function decompress(string $text): string
    {
        return base64_decode($text);
    }
}
