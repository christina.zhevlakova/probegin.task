<?php

namespace App\Actions\Compressors;

use App\Exceptions\InvalidArgumentException;

/**
 * Class CompressorFactory
 * @package App\Actions\Compressors
 */
final class CompressorFactory
{
    const DEFAULT_COMPRESSOR = 'gzip';

    /**
     * @param string $type
     *
     * @return \App\Actions\Compressors\CompressorInterface
     * @throws \App\Exceptions\InvalidArgumentException
     */
    public static function init(string $type = self::DEFAULT_COMPRESSOR): CompressorInterface
    {
        switch($type) {
            case 'uncompress':
                return new UncompressHandler;
            case 'gzip':
                return new GzipHandler;
        }
        throw new InvalidArgumentException('Handler is not defined');
    }

    /**
     * @param string $text
     * @param string $type
     *
     * @return string
     * @throws \App\Exceptions\InvalidArgumentException
     */
    public static function compress(string $text, string $type = self::DEFAULT_COMPRESSOR): string
    {
        return self::init($type)->compress($text);
    }

    /**
     * @param string $text
     * @param string $type
     *
     * @return string
     * @throws \App\Exceptions\InvalidArgumentException
     */
    public static function decompress(string $text, string $type = self::DEFAULT_COMPRESSOR): string
    {
        return self::init($type)->decompress($text);
    }
}
