<?php

namespace App\Actions\Compressors;

/**
 * Interface CompressorInterface
 * @package App\Actions\Compressors
 */
interface CompressorInterface
{
    /**
     * @param string $text
     *
     * @return string
     */
    public function compress(string $text): string;

    /**
     * @param string $text
     *
     * @return string
     */
    public function decompress(string $text): string;
}
