<?php

namespace App\Actions\Compressors;

/**
 * Class GzipHandler
 * @package App\Actions\Compressors
 */
class GzipHandler implements CompressorInterface
{
    /**
     * @param string $text
     *
     * @return string
     */
    public function compress(string $text): string
    {
        return base64_encode(gzcompress($text, 9));
    }

    /**
     * @param string $text
     *
     * @return string
     */
    public function decompress(string $text): string
    {
        return gzuncompress(base64_decode($text));
    }
}
