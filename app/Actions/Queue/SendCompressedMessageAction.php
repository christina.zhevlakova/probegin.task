<?php

namespace App\Actions\Queue;

use App\Actions\Compressors\CompressorFactory;
use Bschmitt\Amqp\Amqp;

/**
 * Class SendCompressedMessageAction
 * @package App\Actions\Queue
 */
class SendCompressedMessageAction
{
    /**
     * @var \Bschmitt\Amqp\Amqp
     */
    private $amqp;

    /**
     * SendCompressedMessageAction constructor.
     *
     * @param \Bschmitt\Amqp\Amqp $amqp
     */
    public function __construct(Amqp $amqp)
    {
        $this->amqp = $amqp;
    }

    /**
     * @param string $queueName
     * @param string $hash
     * @param string $message
     *
     * @throws \App\Exceptions\InvalidArgumentException
     */
    public function run(string $queueName, string $hash, string $message): void
    {
        $message = CompressorFactory::compress($message);

        $data = [
            'hash' => $hash,
            'message' => $message,
        ];

        $this->amqp->publish($queueName, serialize($data), ['queue' => $queueName]);

        return;
    }
}
