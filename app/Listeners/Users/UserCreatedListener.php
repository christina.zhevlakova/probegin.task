<?php

namespace App\Listeners\Users;

use App\Actions\Queue\SendCompressedMessageAction;
use App\Events\Users\UserCreatedEvent;

/**
 * Class UserCreatedListener
 * @package App\Listeners\Users
 */
class UserCreatedListener
{
    /**
     * @var \App\Actions\Queue\SendCompressedMessageAction
     */
    private $sendCompressedMessageAction;

    /**
     * UserCreatedListener constructor.
     *
     * @param \App\Actions\Queue\SendCompressedMessageAction $sendCompressedMessageAction
     */
    public function __construct(SendCompressedMessageAction $sendCompressedMessageAction)
    {
        $this->sendCompressedMessageAction = $sendCompressedMessageAction;
    }

    /**
     * @param \App\Events\Users\UserCreatedEvent $event
     *
     * @throws \App\Exceptions\InvalidArgumentException
     */
    public function handle(UserCreatedEvent $event)
    {
        $this->sendCompressedMessageAction->run(
            'usersCreatedQueue',
            $event->data['hash'],
            $event->data['xml']
        );
    }
}
