<?php

namespace App\Providers;

use App\Events\Users\UserCreatedEvent;
use App\Listeners\Users\UserCreatedListener;
use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

/**
 * Class EventServiceProvider
 * @package App\Providers
 */
class EventServiceProvider extends ServiceProvider
{
    /**
     * @var array
     */
    protected $listen = [
        UserCreatedEvent::class => [
            UserCreatedListener::class,
        ],
    ];
}
