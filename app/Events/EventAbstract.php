<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;

abstract class EventAbstract
{
    use SerializesModels;
}
