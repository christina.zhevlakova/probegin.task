<?php

namespace App\Events\Users;

use App\Events\EventAbstract;

/**
 * Class UserCreatedEvent
 * @package App\Events\Users
 */
class UserCreatedEvent extends EventAbstract
{
    /**
     * @var array
     */
    public $data = [];

    /**
     * UserCreatedEvent constructor.
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }
}
