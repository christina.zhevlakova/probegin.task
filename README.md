Test task

Define a standards 
* Standard format for a JSON request message (JREQ)
- Must have at least 2 attributes 
* Standard format for a XML response message (XREP) 
- Must contain the attributes of JREQ 
- Must contain the an attribute that is mutated 
* Write the database function (postgres/mysql) 
- That does a mutation (like adding or concatenate) 
- Optional the function requires the JREQ parameter 
- Optional the function generates the XREP message 
* Write a service (php /python/...) 
- That consumes a AMQP queue (rabbitmq/zeromq) 
- That expects a zipped JREQ message 
- Call a database function with data from the JREQ 
- Reply via AMQP an answer with a zipped XREP message 
* Write a webpage (php/python & flask optional: php/javascript) 
- That consumes a AMQP answer. 
- That publishes and zipped JREQ request. 
- Optional: Where the fields in the JREQ can be changed in the page with submit 
button 
- That shows the XREP in a response area on the page.
